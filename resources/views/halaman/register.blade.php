@extends('layout.master')

@section ('judul')
Buat Account Baru
@endsection 

@section ('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label> Last Name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="lk"> Laki-Laki <br>
        <input type="radio" name="gender" value="lk"> Perempuan <br>
        <input type="radio" name="gender" value="ot"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nation">
            <option value="1">Indonesian</option>
            <option value="2">America</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa"> Indonesian <br>
        <input type="checkbox" name="bahasa"> English <br>
        <input type="checkbox" name="bahasa"> Other <br><br>
        <label>Bio: </label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>


        <input type="submit" value="Sign Up">
    </form>
@endsection