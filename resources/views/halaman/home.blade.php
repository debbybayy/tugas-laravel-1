@extends('layout.master')

@section ('judul')
Halaman Index
@endsection 

@section ('content')
<h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h4>Benefit Join Di Media Online</h4>

    <ul>
        <li>Mendapat motivasi dari sesama Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web Developer terbaik</li>
    </ul>

    <h4>Cara Bergabung Ke Media Online</h4>

    <ol>
        <li>Mengunjungi Website ini</li>
        <li> Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection